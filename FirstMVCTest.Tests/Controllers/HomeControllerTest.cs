﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using FirstMVCTest;
using FirstMVCTest.Controllers;
using Xunit;

namespace FirstMVCTest.Tests.Controllers
{
    public class HomeControllerTest
    {
        [Fact]
        public void AcceptLanguageEnglish_BrowserRendersHelloWorld()
        {
            // Arrange
            HomeController controller = new HomeController();
            controller.request = new FakeRequest{ Language = "en-US" };

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.Equal("Hello World!", result.ViewBag.Message);
        }

        [Fact]
        public void AcceptLanguageSpanish_BrowserRendersHolaMundo()
        {
            // Arrange
            HomeController controller = new HomeController();
            controller.request = new FakeRequest{ Language = "es-US" };

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.Equal("Hola Mundo!", result.ViewBag.Message);
        }

        [Fact]
        public void AcceptLanguageGerman_ThrowsException()
        {
            // Arrange
            HomeController controller = new HomeController();
            controller.request = new FakeRequest{ Language = "de" };

            // Assert
            Assert.Throws<NotSupportedException>(() => controller.Index());
        }
    }
}
