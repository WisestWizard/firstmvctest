﻿using FirstMVCTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FirstMVCTest.Tests.Controllers
{
    public class FakeRequest : IRequest
    {
        public string Language { get; set; }
    }
}
