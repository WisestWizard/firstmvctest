﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FirstMVCTest.Models
{
    public class Request : IRequest
    {
        public string Language
        {
            get
            {
                return HttpContext.Current.Request.UserLanguages.First();
            }
        }
    }
}