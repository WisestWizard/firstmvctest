﻿using FirstMVCTest.Models;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FirstMVCTest.Controllers
{
    public class HomeController : Controller
    {
        [Inject]
        public IRequest request {get;set;}

        public ActionResult Index()
        {
            if (request.Language.StartsWith("en"))
            {
                ViewBag.Message = "Hello World!";
            }
            else if (request.Language.StartsWith("es"))
            {
                ViewBag.Message = "Hola Mundo!";
            }
            else
            {
                throw new NotSupportedException("Language not supported");
            }

            return View();
        }
    }
}
